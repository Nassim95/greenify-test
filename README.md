# greenify
This software stack was generated with [Compose Generator](https://www.compose-generator.com). <br>
The following sections contain instructions about the selected services and instructions for setting them up.

## Vue frontend
Vue is a client-side JavaScript web framework for developing single page web applications. It works with the MVVM design pattern and supports server side rending since version 2.0.

### Setup
Vue is considered as frontend service and can therefore be found in frontends collection, when generating the compose configuration with Compose Generator.

## Node.js
Node.js can be used for realizing server-side and well scalable applications. It comes with support for JavaScript / TypeScript and is known for its maximized throughput and efficiency by design.

## MongoDB database
MongoDB is a common representative of NoSQL databases / document stores.

### Usage
Compose Generator will setup MongoDB so that an admin and an application database will be created on the first startup of the container. Furthermore an user for your application will be created and granted read/write access to the application database.

### Access manually
To access the data manually, you can use [MongoDBCompass](https://www.mongodb.com/products/compass). It provides you an installable tool to access your local and remote MongoDB instances.

